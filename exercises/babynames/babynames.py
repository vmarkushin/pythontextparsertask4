#!/usr/bin/python3

import re
import sys

years = ["2012", "2010", "2005", "2000", "1990"]


def extract_names(filename):
    babynames = {}
    try:
        text = open(filename, 'r', encoding="UTF-8").read()
    except FileNotFoundError:
        print("Файл не существует")
        sys.exit(1)

    re_td_compiled = re.compile(r'<td.*?>(.*?)</td>', re.DOTALL)
    re_tr_compiled = re.compile(r'<tr.*?>(.*?)</tr>', re.DOTALL)
    re_stat_num_compiled = re.compile(r'(\d+)?')
    re_stat_percent_compiled = re.compile(r'[\d,]+')

    tags_tr = re_tr_compiled.findall(text)
    for tr in tags_tr:
        tags_th_raw = re_td_compiled.findall(tr)
        if not tags_th_raw:
            continue
        tags_th = list(map(
            lambda x: x.strip(),
            tags_th_raw))
        name_index = 1
        rest_info_index = name_index + 1
        name = tags_th[name_index]
        names_dict = {}
        for year, col in zip(years, tags_th[rest_info_index:]):
            found_num = re_stat_num_compiled.match(col)
            stat_num = int(found_num.group(0))
            end_index = found_num.end()
            rest_string = col[end_index:]
            stat_percent = None
            found_percent = re_stat_percent_compiled.search(rest_string)
            if found_percent:
                stat_percent = float(found_percent.group(0).replace(',', '.'))
            names_dict[year] = [stat_num, stat_percent]
        babynames[name] = names_dict
    return babynames


def print_names(babynames):
    hint = "Введите год ({}): ".format(", ".join(years))
    hint_continue = "Продолжить? (Y/n)"

    print(hint)

    for line in map(str.rstrip, sys.stdin):
        if line == 'q':
            sys.exit(0)
        elif line in years:
            year = line
        else:
            print(hint)
            continue

        sorted_items = sorted(map(
            lambda x: (x[0], x[1][year]), babynames.items()),
            key=lambda x: (len(x[0].split()), x[1][0]),
            reverse=True)

        sorted_items_list = list(sorted_items)
        max_name_length = max(map(lambda x: len(x[0]), sorted_items_list))
        additional_align = 2
        format_str = "{{:<{}}}{{}}".format(max_name_length + additional_align)
        for i in sorted_items_list:
            print(format_str.format(i[0], i[1]))

        print(hint_continue)
        for c in map(str.rstrip, sys.stdin):
            if c == 'y':
                break
            elif c == 'n':
                sys.exit(0)
            print(hint_continue)
        print(hint)


def main():
    args = sys.argv[1:]

    if not args:
        print('usage: filename')
        sys.exit(1)

    filename = args[0]
    babynames = extract_names(filename)
    print_names(babynames)


if __name__ == '__main__':
    main()
