#!/usr/bin/python3

import os
import re
import sys
import urllib.request


def read_urls(filename):
    try:
        text = open(filename, 'r', encoding="UTF-8").read()
    except FileNotFoundError:
        print("Файл не существует")
        sys.exit(1)
    domain = re.search(r'[\w-]*?\.[a-zA-Z]{2,6}', filename).group(0)
    image_urls = list(
        map(lambda x: "http://{}{}".format(domain, x),
            sorted(
                set(re.findall(r'/images/animals_\d\d\.jpg', text, re.DOTALL)))))
    return image_urls


def download_images(img_urls, dest_dir):
    try:
        os.makedirs(dest_dir)
    except FileExistsError:
        pass

    index = 0
    images_names = []

    for url in img_urls:
        index += 1
        image_name = "img{:02}.jpg".format(index)
        images_names.append(image_name)
        urllib.request.urlretrieve(url, "{}/{}".format(dest_dir, image_name))

    images_names_html_tags = map(lambda x: "<img src=\"{}\"></img>".format(x), images_names)
    html = "<html><body>{}</body></html>".format(''.join(images_names_html_tags))
    html_file_path = "{}/index.html".format(dest_dir)

    open(html_file_path, "w+").write(html)


def main():
    args = sys.argv[1:]

    if not args:
        print('usage: [--todir dir] logfile')
        sys.exit(1)

    todir = ''
    if args[0] == '--todir':
        todir = args[1]
        del args[0:2]

    img_urls = read_urls(args[0])

    if todir:
        download_images(img_urls, todir)
    else:
        print('\n'.join(img_urls))


if __name__ == '__main__':
    main()
